package lhy.JubeatInfo;


import java.text.DecimalFormat;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
 
public class UserInfo extends Activity{
	private Facebook mFacebook = new Facebook(Val.FACEBOOK_APP_ID);
	private ImageView mFacebookBtn;
	private TextView tvFacebook;

	public TextView ID;
	public TextView Title;
	public TextView GroupName;
	public TextView Jubility;
	public TextView Marker;
	public TextView Background;
	public TextView AchievementPoint;
	public TextView TotalBestScore;
	public TextView PlayCount;
	public TextView ClearCount;
	public TextView FullComboCount;
	public TextView ExcellentCount;
	public TextView FullComboTuneCount;
	public TextView ExcellentTuneCount;
	public TextView SaveCount;
	public TextView SavedCount;
	public TextView AddictionLevel;
	public TextView ConcentrationLevel;
	public TextView EfficiencyLevel;
	public TextView LastDate;
	//public TextView LastCountry;
	public TextView LastRegion;
	public TextView JubilityIcon;

	public String JubeatInfoUrl;
	public String JubilityIconUrl;
	public String MarkerUrl;
	public String BackgroundUrl;

	@Override
	public void onCreate(Bundle savedInstanceState){
		doTitlebar();//타이틀바 색상 바꾸기 함수
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.userinfo);

		DecimalFormat decimal1000 = new DecimalFormat("###,###");

		String UserID = getIntent().getStringExtra("UserID");
		String FrontUrl = "http://j.ubeat.info/copious/";
		String BackUrl = "/stat?fmt=json&lang=";
		Locale lc = getResources().getConfiguration().locale;
		String Local = lc.getLanguage();
		if(Local.equals("ko") || Local.equals("KO") || Local.equals("ko-kr")) {Local = "ko";}
		else if(Local.equals("Ja") || Local.equals("ja") || Local.equals("ja-jp")) {Local = "ja";}
		else if(Local.equals("zh") || Local.equals("Zh") || Local.equals("Zh-tw")) {Local = "zh-tw";}
		else {Local = "en";}

		String Url = FrontUrl + UserID + BackUrl + Local;

		JSONObject CurUser = JsonFunc.getJSONfromURL(Url);

		JubeatInfoUrl = "http://j.ubeat.info";
		JubilityIconUrl = null;
		MarkerUrl = null;
		BackgroundUrl = null;

		try{
			//각 정보 TextView의 변수 연결
			ID = (TextView)findViewById(R.id.UserInfo_CurUserID);
			Title = (TextView)findViewById(R.id.UserInfo_Title);
			GroupName = (TextView)findViewById(R.id.UserInfo_GroupName);
			Jubility = (TextView)findViewById(R.id.UserInfo_Jubility);
			Marker = (TextView)findViewById(R.id.UserInfo_Marker);
			Background = (TextView)findViewById(R.id.UserInfo_Background);
			AchievementPoint = (TextView)findViewById(R.id.UserInfo_AchievementPoint);
			TotalBestScore = (TextView)findViewById(R.id.UserInfo_TotalBestScore);
			PlayCount = (TextView)findViewById(R.id.UserInfo_PlayCount);
			ClearCount = (TextView)findViewById(R.id.UserInfo_ClearCount);
			FullComboCount = (TextView)findViewById(R.id.UserInfo_FullComboCount);
			ExcellentCount = (TextView)findViewById(R.id.UserInfo_ExcellentCount);
			FullComboTuneCount = (TextView)findViewById(R.id.UserInfo_FullComboTuneCount);
			ExcellentTuneCount = (TextView)findViewById(R.id.UserInfo_ExcellentTuneCount);
			SaveCount = (TextView)findViewById(R.id.UserInfo_SaveCount);
			SavedCount = (TextView)findViewById(R.id.UserInfo_SavedCount);
			AddictionLevel = (TextView)findViewById(R.id.UserInfo_AddictionLevel);
			ConcentrationLevel = (TextView)findViewById(R.id.UserInfo_ConcentrationLevel);
			EfficiencyLevel = (TextView)findViewById(R.id.UserInfo_EfficiencyLevel);
			LastDate = (TextView)findViewById(R.id.UserInfo_LastDate);
			//LastCountry;
			LastRegion = (TextView)findViewById(R.id.UserInfo_LastRegion);
			JubilityIcon = (TextView)findViewById(R.id.UserInfo_JubilityIcon);

			//각 정보 TextView에 Data삽입
			ID.setText(CurUser.getString("player_name"));
			Title.setText(CurUser.getString("title"));
			if(CurUser.getString("group_name") == "null"){
				GroupName.setText("No Group");
			}else{
				GroupName.setText(CurUser.getString("group_name"));
			}
			Jubility.setText(CurUser.getString("jubility"));
			Marker.setText(CurUser.getString("marker"));
			Background.setText(CurUser.getString("background"));
			AchievementPoint.setText(decimal1000.format(CurUser.getInt("achievement_point")));
			TotalBestScore.setText(decimal1000.format(CurUser.getInt("total_best_score")));
			PlayCount.setText(CurUser.getString("play_count"));
			ClearCount.setText(CurUser.getString("clear_count"));
			FullComboCount.setText(CurUser.getString("fullcombo_count"));
			ExcellentCount.setText(CurUser.getString("excellent_count"));
			FullComboTuneCount.setText(CurUser.getString("fullcombo_tune_count"));
			ExcellentTuneCount.setText(CurUser.getString("excellent_tune_count"));
			SaveCount.setText(CurUser.getString("save_count"));
			SavedCount.setText(CurUser.getString("saved_count"));
			String Addiction = String.format("%.1f", CurUser.getDouble("balanced_addiction_level")*100) + "%";
			AddictionLevel.setText(Addiction);
			ConcentrationLevel.setText(CurUser.getString("concentration_level"));
			EfficiencyLevel.setText(String.format("%.0f", CurUser.getDouble("efficiency_level")));
			//LastCountry.setText(CurUser.getString("last_country"));
			LastDate.setText(CurUser.getString("last_date"));
			LastRegion.setText(CurUser.getString("last_region"));
			JubilityIcon.setText(CurUser.getString("jubility_icon"));

			String MarkerPath = CurUser.getString("marker_path");
			String JubilityIconPath = CurUser.getString("jubility_icon_path");
			String BackgroundPath = CurUser.getString("background_path");

			JubilityIconUrl = JubeatInfoUrl + JubilityIconPath;
			MarkerUrl = JubeatInfoUrl + MarkerPath;
			BackgroundUrl = JubeatInfoUrl + BackgroundPath;

			JSONObject CurUserLocal = CurUser.getJSONObject("localization");



			//제목 Localization변수와 TextView연결
			TextView AchievementPointName = (TextView)findViewById(R.id.UserInfo_AchievementPointName);
			//TextView AchievementRankName;
			TextView BackgroundName = (TextView)findViewById(R.id.UserInfo_BackgroundName);
			TextView AddictionLevelName = (TextView)findViewById(R.id.UserInfo_AddictionLevelName);
			//TextView CardNameName = (TextView)findViewById(R.id.UserInfo_CardNameName);
			TextView ClearCountName = (TextView)findViewById(R.id.UserInfo_ClearCountName);
			//TextView ClearRateName = (TextView)findViewById(R.id.UserInfo_ClearRateName);
			TextView ConcentrationLevelName = (TextView)findViewById(R.id.UserInfo_ConcentrationLevelName);
			//TextView EffectiveExcellentRateName = (TextView)findViewById(R.id.UserInfo_EffectiveExcellentRateName);
			//TextView EffectiveFullcomboRateName = (TextView)findViewById(R.id.UserInfo_EffectiveFullcomboRateName);
			TextView EfficiencyLevelName = (TextView)findViewById(R.id.UserInfo_EfficiencyLevelName);
			TextView ExcellentCountName = (TextView)findViewById(R.id.UserInfo_ExcellentCountName);
			//TextView ExcellentRateName = (TextView)findViewById(R.id.UserInfo_ExcellentRateName);
			TextView ExcellentTuneCountName = (TextView)findViewById(R.id.UserInfo_ExcellentTuneCountName);
			TextView FullComboCountName = (TextView)findViewById(R.id.UserInfo_FullComboCountName);
			//TextView FullComboRateName = (TextView)findViewById(R.id.UserInfo_FullComboRateName);
			TextView FullComboTuneCountName = (TextView)findViewById(R.id.UserInfo_FullComboTuneCountName);
			TextView GroupNameName = (TextView)findViewById(R.id.UserInfo_GroupNameName);
			TextView JubilityName = (TextView)findViewById(R.id.UserInfo_JubilityName);
			//TextView LastDateName = (TextView)findViewById(R.id.UserInfo_LastPlayName);
			//TextView LastRegionName = (TextView)findViewById(R.id.UserInfo_LastRegionName);
			TextView MarkerName = (TextView)findViewById(R.id.MarkerName);
			////TextView MatchedPlayerCountName = (TextView)findViewById(R.id.UserInfo_MatchedPlayerCountName);
			////TextView MatchedPlayerRateName = (TextView)findViewById(R.id.UserInfo_MatchedPlayerRateName);
			////TextView MatchedVictoryCountName = (TextView)findViewById(R.id.UserInfo_MatchedVictoryCountName);
			////TextView MatchedVictoryRateName = (TextView)findViewById(R.id.UserInfo_MatchedVictoryRateName);
			TextView PlayCountName = (TextView)findViewById(R.id.UserInfo_PlayCountName);
			//TextView PlayerNameName = (TextView)findViewById(R.id.UserInfo_PlayerNameName);
			TextView SaveCountName = (TextView)findViewById(R.id.UserInfo_SaveCountName);
			TextView SavedCountName = (TextView)findViewById(R.id.UserInfo_SavedCountName);
			////TextView SavedRateName = (TextView)findViewById(R.id.UserInfo_SavedRateName);
			TextView TitleName = (TextView)findViewById(R.id.UserInfo_TitleName);
			TextView TotalBestScoreName = (TextView)findViewById(R.id.UserInfo_TotalBestScoreName);
			////TextView TotalBestScoreRankName = (TextView)findViewById(R.id.UserInfo_TotalBestScoreRankName);


			AchievementPointName.setText(CurUserLocal.getString("achievement_point"));
			//AchievementRankName.setText(CurUserLocal.getString("achievement_rank"));
			BackgroundName.setText(CurUserLocal.getString("background"));
			AddictionLevelName.setText(CurUserLocal.getString("balanced_addiction_level"));
			//CardNameName.setText(CurUserLocal.getString("card_name"));
			ClearCountName.setText(CurUserLocal.getString("clear_count"));
			//ClearRateName.setText(CurUserLocal.getString("clear_rate"));
			ConcentrationLevelName.setText(CurUserLocal.getString("concentration_level"));
			//EffectiveExcellentRateName.setText(CurUserLocal.getString("effective_excellent_rate"));
			//EffectiveFullComboRateName.setText(CurUserLocal.getString("effective_fullcombo_rate"));
			EfficiencyLevelName.setText(CurUserLocal.getString("efficiency_level"));
			//ExcellentRateName.setText(CurUserLocal.getString("excellent_rate"));
			ExcellentCountName.setText(CurUserLocal.getString("excellent_count"));
			ExcellentTuneCountName.setText(CurUserLocal.getString("excellent_tune_count"));
			FullComboCountName.setText(CurUserLocal.getString("fullcombo_count"));
			//FullComboRateName.setText(CurUserLocal.getString("fullcombo_rate"));
			FullComboTuneCountName.setText(CurUserLocal.getString("fullcombo_tune_count"));
			GroupNameName.setText(CurUserLocal.getString("group_name"));
			JubilityName.setText(CurUserLocal.getString("jubility"));
			//LastDateName.setText(CurUserLocal.getString("last_date"));
			MarkerName.setText(CurUserLocal.getString("marker"));
			//MatchedPlayerCountName.setText(CurUserLocal.getString("matched_player_count"));
			//MatchedPlayerRateName.setText(CurUserLocal.getString("matched_player_rate"));
			//MatchedVictoryCountName.setText(CurUserLocal.getString("matched_victory_count"));
			//MatchedVictoryRateName.setText(CurUserLocal.getString("matched_victory_rate"));
			PlayCountName.setText(CurUserLocal.getString("play_count"));
			//PlayerNameName.setText(CurUserLocal.getString("player_name"));
			SaveCountName.setText(CurUserLocal.getString("save_count"));
			SavedCountName.setText(CurUserLocal.getString("saved_count"));
			//SavedRateName.setText(CurUserLocal.getString("saved_rate"));
			TitleName.setText(CurUserLocal.getString("title"));
			TotalBestScoreName.setText(CurUserLocal.getString("total_best_score"));
			//TotalBestScoreRankName.setText(CurUserLocal.getString("total_best_score_rank"));

			//유저정보를 보고난 후 유저리스트의 간략정보 갱신을위한 인텐트 전달(텍스트). 그림은 아래쪽에서 추가하고 setResult호출
			Intent ToUserList = getIntent();

			ToUserList.putExtra("CardName", CurUser.getString("card_name"));
			ToUserList.putExtra("LastPlay", CurUser.getString("last_date"));
			ToUserList.putExtra("JubilityIconUrl", JubilityIconUrl);
			ToUserList.putExtra("Title", CurUser.getString("title"));
			ToUserList.putExtra("GroupName", GroupName.getText());

			setResult(RESULT_OK, ToUserList);

		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data "+e.toString());
		}


		ImageView JubilityIcon = (ImageView)findViewById(R.id.JubilityIcon);
		ImageView BackgroundIcon = (ImageView)findViewById(R.id.BackgroundIcon);
		ImageView Marker = (ImageView)findViewById(R.id.Marker);

		final ImageDownloader imageDownloader = new ImageDownloader();
		imageDownloader.download(JubilityIconUrl, JubilityIcon);
		imageDownloader.download(BackgroundUrl, BackgroundIcon);
		imageDownloader.download(MarkerUrl, Marker);
		/*
		mFacebookBtn = (ImageView)findViewById(R.id.btnFacebook);
		mFacebookBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				facebookLogin();
				facebookFeed();
			}
		});
		
		tvFacebook = (TextView)findViewById(R.id.tvFacebook);
		tvFacebook.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				facebookFeed();
			}
		});
		*/
	}
	
	private void facebookLogin(){
		mFacebook.authorize2(this, new String[] {"publish_stream, user_photos, email"}, new AuthorizeListener());
	}

	public class AuthorizeListener implements DialogListener{
		public void onCancel() {}
		public void onComplete(Bundle values){
			if (Val.D) Log.v(Val.LOG_TAG, "::: onComplete :::");
		}
		//public void onError(DialogError e) {}
		//public void onFacebookError(FacebookError e) {}
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub
			
		}
		public void onError(DialogError e) {
			// TODO Auto-generated method stub
			
		}
	}

	private void facebookFeed(){
		try{
			Log.v(Val.LOG_TAG, "access token : " + mFacebook.getAccessToken());

			String post = "ID : " + ID.getText().toString() + "\n"
				+ "Jubility : " + Jubility.getText().toString() + "\n"
				+ "AchivementPoint : " + AchievementPoint.getText().toString() + "\n"
				+ "TotalBestScore : " + TotalBestScore.getText().toString() + "\n"
				+ "PlayCount : " + PlayCount.getText().toString() + "\n"
				+ "Addiction Level : " + AddictionLevel.getText() + "\n";

			Bundle params = new Bundle();
			params.putString("message", "ABCD");
			params.putString("name", "JubeatInfo name:" + ID.getText().toString());
			params.putString("link", "market://details?id=lhy.JubeatInfo");
			params.putString("description", "JubeatInfo Android를 통해 포스트됨");
			//params.putString("picture", JubilityIconUrl);

			mFacebook.request("me/feed", params, "POST");
			
			Toast.makeText(UserInfo.this, "포스팅 완료", Toast.LENGTH_SHORT);
			Log.v(Val.LOG_TAG, "posting complete");
			}
		catch (Exception e){
			Toast.makeText(UserInfo.this, "포스팅 실패", Toast.LENGTH_SHORT);
			e.printStackTrace();
		}

	}

	//타이틀바 색상 바꾸기
	private void doTitlebar()
	{
		View titleView = getWindow().findViewById(android.R.id.title);
		if (titleView != null)
		{
			ViewParent parent = titleView.getParent();
			if (parent != null && (parent instanceof View))
			{
				View parentView = (View) parent;
				parentView.setBackgroundResource(R.drawable.titlebar);
			}
		}
	}

}