package lhy.JubeatInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserAdd extends Activity{
	EditText JubeatInfoName;
	Intent intent = getIntent();
	String Local;
	String enrollResult;
	@Override
	public void onCreate(Bundle savedInstanceState){
		doTitlebar();//타이틀바 색상 바꾸기 함수
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.useradd);


		final String FrontUrl = "http://j.ubeat.info/copious/";
		final String BackUrl = "?fmt=json";

		Locale lc = getResources().getConfiguration().locale;
		Local = lc.getLanguage();
		if(Local.equals("ko") || Local.equals("KO") || Local.equals("ko-kr")) {Local = "ko";}
		else if(Local.equals("Ja") || Local.equals("ja") || Local.equals("ja-jp")) {Local = "ja";}
		else if(Local.equals("zh") || Local.equals("Zh") || Local.equals("Zh-tw")) {Local = "zh-tw";}
		else {Local = "en";}

		Button AddUser = (Button)findViewById(R.id.AddUser_btn);
		Button Enroll = (Button)findViewById(R.id.Enroll_btn);

		JubeatInfoName = (EditText)findViewById(R.id.JubeatInfoName_editText);



		//엔터입력시 AddUser버튼과 같은효과
		JubeatInfoName.setOnKeyListener(new OnKeyListener(){
			public boolean onKey(View v, int keyCode, KeyEvent event){
				{
					if(keyCode==KeyEvent.KEYCODE_ENTER){
						if(JubeatInfoName.length()==0){
							Toast.makeText(UserAdd.this, "Input JubeatInfo Name" , Toast.LENGTH_SHORT).show();
						}
						else{
							String UserID = JubeatInfoName.getText().toString();
							String Url = FrontUrl + UserID + BackUrl + "&" + Local;

							JSONObject CurUser = JsonFunc.getJSONfromURL(Url);

							AddUser(CurUser);
						}
					}else{
						return false;
					}
				}
				return true;
			}
		});

		//---------------------------------------------------
		//		AddUser Btn 클릭 시
		//---------------------------------------------------
		AddUser.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				if(JubeatInfoName.length()==0){
					Toast.makeText(UserAdd.this, "Input JubeatInfo Name" , Toast.LENGTH_SHORT).show();
				}
				else{
					String UserID = JubeatInfoName.getText().toString();
					String Url = FrontUrl + UserID + BackUrl + "&" + Local;;

					JSONObject CurUser = JsonFunc.getJSONfromURL(Url);


					AddUser(CurUser);
				}
			}
		});

		//---------------------------------------------------
		//		Enroll Btn 클릭시
		//---------------------------------------------------
		Enroll.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				EditText JubeatRivalID = (EditText)findViewById(R.id.JubeatRivalID_editText);
				if(JubeatRivalID.length()==0){
					Toast.makeText(UserAdd.this, "Input JubeatRivalID" , Toast.LENGTH_SHORT).show();
				}
				else{
					if(JubeatInfoName.length()==0)
						Toast.makeText(UserAdd.this, "Input JubeatInfo Name", Toast.LENGTH_SHORT).show();
					else{
						String ID = JubeatInfoName.getText().toString();
						String Rival = JubeatRivalID.getText().toString();
						HttpPostEnroll(ID, Rival);
						Toast.makeText(UserAdd.this, enrollResult, Toast.LENGTH_LONG).show();
						/*
						intent.putExtra("RivalID", JubeatRivalID.getText().toString());
						setResult(RESULT_OK, intent);
						finish();
						*/
					}
				}
			}
		});
	}

	
	//----------------------------------------------------------
	//		HttpPostEnroll
	//----------------------------------------------------------
	public void HttpPostEnroll(String JubeatInfoID, String RivalID){
		try{
			//URL설정&접속
			URL url = new URL("http://j.ubeat.info/copious/+/enroll");	//URL설정
			HttpURLConnection http = (HttpURLConnection)url.openConnection();	//접속

			//전송모드 설정
			http.setDefaultUseCaches(false);
			http.setDoInput(true);				//서버에서 읽기모드 지정
			http.setDoOutput(true);				//서버에서 쓰기모드 지정
			http.setRequestMethod("POST");		//전송방식  POST

			// 서버에게 웹에서 <Form>으로 값이 넘어온 것과 같은 방식으로 처리하라는 걸 알려준다
			//http.setRequestProperty("content-type", "application/x-www-form-urlencoded");

			//서버로 값 전송
			StringBuffer buffer = new StringBuffer();
			buffer.append("alias").append("=").append(JubeatInfoID).append("&");
			buffer.append("friend_id").append("=").append(RivalID);
			buffer.append("&").append("fmt=json");
			buffer.append("&").append("lang=").append(Local);

			OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "UTF-8");
			PrintWriter writer = new PrintWriter(outStream);
			writer.write(buffer.toString());
			writer.flush();

			//서버에서 전송받기 
			InputStreamReader tmp = new InputStreamReader(http.getInputStream(), "UTF-8");
			BufferedReader reader = new BufferedReader(tmp);
			StringBuilder builder = new StringBuilder();
			String str;
			while((str=reader.readLine())!=null){
				builder.append(str + "\n");
			}
			//enrollResult = builder.toString();
			//String temp = builder.toString();
			//Toast.makeText(UserAdd.this, enrollResult, Toast.LENGTH_LONG).show();
			Toast.makeText(UserAdd.this, "Enroll Success\nYour data will be updated in next minor update time", Toast.LENGTH_LONG).show();
		}catch(MalformedURLException e){
			Toast.makeText(UserAdd.this, "Enroll Error", Toast.LENGTH_LONG).show();
		}catch(IOException e){
			Toast.makeText(UserAdd.this, "Enroll Error\nMaybe Non-Existent RivalID\n or \nJubeatInfo Name is already in use", Toast.LENGTH_LONG).show();
		}
	}

	//---------------------------------------------
	//		AddUser
	//---------------------------------------------
	//JSONObject를 인자로받아 유저정보 인텐트로 전달
	private void AddUser(JSONObject CurUser){
		Intent intent = getIntent();

		try{
			String Error = CurUser.getString("error");
			Toast.makeText(UserAdd.this, JubeatInfoName.getText().toString() + " is not enrolled to JubeatInfo service", Toast.LENGTH_LONG).show();
			//Toast.makeText(UserAdd.this, "사용자이름이 서비스에 등록되어있지않습니다", Toast.LENGTH_LONG).show();
		}catch(JSONException e){
			try{						
				String CardName = CurUser.getString("card_name");
				String Title = CurUser.getString("title");
				String GroupName = CurUser.getString("group_name");
				if(GroupName.equals("null")){ GroupName = "No Group"; }
				String JubeatInfoUrl = "http://j.ubeat.info";
				String JubilityIconPath = CurUser.getString("jubility_icon_path");
				String JubilityIconUrl = JubeatInfoUrl + JubilityIconPath;

				intent.putExtra("UserID", JubeatInfoName.getText().toString());
				intent.putExtra("CardName", CardName);
				intent.putExtra("Title", Title);
				intent.putExtra("GroupName", GroupName);
				intent.putExtra("JubilityIconUrl", JubilityIconUrl);

				setResult(RESULT_OK, intent);
				finish();
			}catch(JSONException e2){}
		}
	}

	//타이틀바
	private void doTitlebar()
	{
		View titleView = getWindow().findViewById(android.R.id.title);
		if (titleView != null)
		{
			ViewParent parent = titleView.getParent();
			if (parent != null && (parent instanceof View))
			{
				View parentView = (View) parent;
				parentView.setBackgroundResource(R.drawable.titlebar);
			}
		}
	}
}