//----------------------------------------------------
//		ScoreList - 각 유저의 곡스코어 보여줌
//----------------------------------------------------

package lhy.JubeatInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import lhy.JubeatInfo.UserList.User;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ScoreList extends Activity{
	private ArrayList<Score> Scorelist;					// 모든 점수정보 저장리스트
	private ArrayList<Score> Scorelist_Exist;			// 플레이 된 곡만 저장된 리스트
	private ScoreListArrayAdapter slaa = null;			// 모든곡 어댑터
	private ScoreListArrayAdapter slaa_e = null;		// 플레이 된 곡 어댑터
	private ArrayList<Comparator<Score>> comparators;	// Score비교
	private ListView listview;							// 전체 리스트뷰
	private int exist = 0;								// 플레이 된 곡 여부 판단 전역변수
	private String Local;								// Localization텍스트 저장 변수
	private AlertDialog dlg;							// 
	private String updateMessage;						// 서버로부터 전달되는 메세지 저장 변수

	@Override
	public void onCreate(Bundle savedInstanceState){
		doTitlebar();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scorelist);


		final String UserID = getIntent().getStringExtra("UserID");	// Intent로 전달된 UserID 변수에 저장
		String FrontUrl = "http://j.ubeat.info/copious/";
		String BackUrl = "/score?fmt=json&lang=";

		Locale lc = getResources().getConfiguration().locale;
		Local = lc.getLanguage();
		if(Local.equals("ko") || Local.equals("KO") || Local.equals("ko-kr")) {Local = "ko";}
		else if(Local.equals("Ja") || Local.equals("ja") || Local.equals("ja-jp")) {Local = "ja";}
		else if(Local.equals("zh") || Local.equals("Zh") || Local.equals("Zh-tw")) {Local = "zh-tw";}
		else {Local = "en";}

		String Url = FrontUrl + UserID + BackUrl + Local;		// 전부 합쳐서 사이트에 요청

		Scorelist = new ArrayList<Score>();
		Scorelist_Exist = new ArrayList<Score>();

		/*테스트용
		Score AddScore = new Score("artist", "bpm", "title", "version", "copious_offset", "flag", "sort_id",
				"level", "notes", "score", "rating", "date");
		Scorelist.add(AddScore);
		 */

		listview = (ListView)findViewById(R.id.ScoreList_ListView);

		slaa = new ScoreListArrayAdapter(this, R.layout.scorelist_row, Scorelist);
		slaa_e = new ScoreListArrayAdapter(this, R.layout.scorelist_row, Scorelist_Exist);
		//?, 각 row의 레이아웃, 전달할 리스트 인자로 전달
		listview.setAdapter(slaa);
		//커스텀ArrayAdapter선언/초기화, 본Activity Adapter로 ulaa지정



		//-----------------------------------------------------------------
		//		JSON전달받아 JSONArray에 저장
		//-----------------------------------------------------------------
		JSONObject CurUser = JsonFunc.getJSONfromURL(Url);
		JSONObject Scores = null;
		try{
			Scores = CurUser.getJSONObject("scores");

			JSONArray ScoresID = Scores.names();		//Scores의 name개수로 전달받은 JSONObject의 길이 저장

			for(int i=0; i<ScoresID.length(); i++){
				String ID = ScoresID.getString(i);
				JSONObject track = Scores.getJSONObject(ID);

				String artist = track.getString("artist");
				//String bpm = track.getString("bpm_max");
				String title = track.getString("title");
				int version = track.getInt("version");
				//int copious_offset = track.getInt("copious_offset");
				//int flag = track.getInt("flags");
				int sort_id = track.getInt("sort_id");
				String localization = track.getString("localization");

				
				// 3개 난이도별로 나머지정보 받아옴
				String[] difs = {"bsc", "adv", "ext"};
				for (String dif : difs) {
					JSONObject tune = null;
					try{
						tune = track.getJSONObject(dif);
					}catch(JSONException e){
						Log.e("log_tag", "Error parsing data "+e.toString());
					}
					int level = tune.getInt("level");
					//					String notes = tune.getString("notes");
					JSONObject tuneData = tune.getJSONObject("data");
					String difficulty = dif.toUpperCase(); //대문자
					int score = tuneData.getInt("score");
					String rating = tuneData.getString("rating");
					//time = BscData.getString("time");
					String date = tuneData.getString("date_score");
					Score aScore = new Score(artist, title, version, sort_id, difficulty,
							score, level, rating, date, localization);
					
					//rating이 있을경우(플레이 기록이 있을경우) - Scorelist_Exist리스트에 저장
					if(rating.equals("NO") == false) { Scorelist_Exist.add(aScore); }

					//전체 Scorelist 리스트에 저장
					Scorelist.add(aScore);

				}
			}
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data "+e.toString());
		}

		//Toast.makeText(this, "Endddddddddddddddddddddddd" , Toast.LENGTH_LONG).show();

		initButtons();
		Collections.sort(Scorelist, comparators.get(0));

		//---------------------------------------------------------------
		//		FilterBtn클릭시 - 기록있는곡 리스트로 바꾸어 보여줌
		//--------------------------------------------------------------
		final TextView FilterBtn = (TextView)findViewById(R.id.ScoreList_FilterBtn);
		FilterBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				if(exist == 0){
					listview.setAdapter(slaa_e);
					slaa_e.notifyDataSetChanged();
					exist = 1;
					FilterBtn.setText(R.string.scorelistFilterExist0);
				}
				else{
					listview.setAdapter(slaa);
					slaa.notifyDataSetChanged();
					exist = 0;
					FilterBtn.setText(R.string.scorelistFilterExist1);
				}
			}
		});
		
		

		//-------------------------------------------------------------------
		//		updateBtn클릭시 - HttpPostUpdate실행
		//-------------------------------------------------------------------
		TextView updateBtn = (TextView)findViewById(R.id.ScoreList_UpdateBtn);
		updateBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				HttpPostUpdate(UserID);
				//getJSONUpdate(UserID);
			}
		});
	}
/*
	//갱신요청JSON으로
	public static JSONObject getJSONUpdate(String UserID){
		InputStream is = null;
		OutputStream os = null;
		String result = "";
		JSONObject jArray = null;

		//http post
		try{
			String url = "http://j.ubeat.info/copious/" + UserID + "/request";	//URL설정
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url + "?alias=" + UserID);
/*			
			//서버로 값 전송
			StringBuffer buffer = new StringBuffer();
			buffer.append("alias").append("=").append(UserID);
			buffer.append("&").append("fmt=json");
			//buffer.append("&").append("lang=").append(Local);

			OutputStreamWriter outStream = new OutputStreamWriter(os, "UTF-8");
			PrintWriter writer = new PrintWriter(outStream);
			writer.write(buffer.toString());
			writer.flush();
			/////////////////////////
	//*
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		}catch(Exception e){
			Log.e("log_tag", "Error in http connection "+e.toString());
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			String temp = sb.toString();
			result=sb.toString();
		}catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		try{
			JSONObject temp2 = new JSONObject(result);
			jArray = new JSONObject(result);            
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data "+e.toString());
		}

		return jArray;
	}
	
*/
	//----------------------------------------------------------------------
	//		HttpPostUpdate - Update요청
	//----------------------------------------------------------------------
	public void HttpPostUpdate(String JubeatInfoID){
		JSONObject jsonTemp = null;
		try{
			//URL설정&접속
			URL url = new URL("http://j.ubeat.info/copious/" + JubeatInfoID + "/request?fmt=json&lang=" + Local);	//URL설정
			HttpURLConnection http = (HttpURLConnection)url.openConnection();	//접속

			//전송모드 설정
			http.setDefaultUseCaches(false);
			http.setDoInput(true);				//서버에서 읽기모드 지정
			http.setDoOutput(true);				//서버에서 쓰기모드 지정
			//http.setAllowUserInteraction(true);
			http.setRequestMethod("POST");		//전송방식  POST
		

			// 서버에게 웹에서 <Form>으로 값이 넘어온 것과 같은 방식으로 처리하라는 걸 알려준다
			//http.setRequestProperty("content-type", "application/x-www-form-urlencoded");

			//서버로 값 전송
			StringBuffer buffer = new StringBuffer();
			buffer.append("alias").append("=").append(JubeatInfoID);

			OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "UTF-8");
			PrintWriter writer = new PrintWriter(outStream);
			writer.write(buffer.toString());
			writer.flush();

			//서버에서 전송받기 
			InputStreamReader tmp = new InputStreamReader(http.getInputStream(), "UTF-8");
			BufferedReader reader = new BufferedReader(tmp);
			StringBuilder builder = new StringBuilder();
			String str;
			while((str=reader.readLine())!=null){
				builder.append(str + "\n");
			}
			//enrollResult = builder.toString();
			String temp = builder.toString();
			try{
				jsonTemp = new JSONObject(temp);
			}catch(JSONException e){
				Log.e("ToJSON", "Error parsing data" + e.toString());
			}
			JSONObject temp2 = jsonTemp;
			
			updateMessage = builder.toString();
			JSONObject curJson = new JSONObject(updateMessage);
			if((curJson.getString("success")).equals("1")){
				Toast.makeText(ScoreList.this, "Your update request is Accepted", Toast.LENGTH_LONG).show();
				showDialog(0);
			}
			//Toast.makeText(UserAdd.this, enrollResult, Toast.LENGTH_LONG).show();
		}catch(MalformedURLException e){
			Toast.makeText(ScoreList.this, "Request error", Toast.LENGTH_LONG).show();
			showDialog(1);
		}catch(IOException e){
			Toast.makeText(this, "Update has been requested already", Toast.LENGTH_LONG).show();
			showDialog(2);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//--------------------------------------------------------------------------
	//		Dialog 설정
	//			만들어질 Dialog의 형태 결정 <- 이 함수로 만들어짐(dlg = 팝업창)
	//--------------------------------------------------------------------------
	@Override
	protected Dialog onCreateDialog(int id){
		super.onCreateDialog(id);
		//스위치문으로 여러개의 Dialog만들어놓고 필요한것을 부를수있음
		switch(id){
		case 0:
			dlg = new AlertDialog.Builder(this)
			//.setTitle("Edit UserInfo")
			.setMessage("Update Request Success")
			//.setMessage(updateMessage)
			//.setMessage("Your update request is Accepted")
			//.setView(createCustomView())
			.create();
			break;
		case 1:
			dlg = new AlertDialog.Builder(this)
			//.setTitle("Edit UserInfo")
			.setMessage("Connection Error")
			//.setMessage("Request error")
			//.setView(createCustomView())
			.create();
			break;
		case 2:
			dlg = new AlertDialog.Builder(this)
			//.setTitle("Edit UserInfo")
			.setMessage("Error Occured\nor Requested Already")
			//.setMessage("Update has been request already")
			//.setView(createCustomView())
			.create();
			 break;
		default:
			dlg = null;
		}
		return dlg;
	}

	//----------------------------------------------------------------------
	//		onPrepareDialog
	//			처음 생성된 AlertDialog 객체이건 재사용되는 AlerDialog 객체이건
	//			AlertDialog객체가 화면에 표시 될때 마다 항상 호출 됨
	//----------------------------------------------------------------------
	@Override
	public void onPrepareDialog(int id, Dialog dlg) {
		super.onPrepareDialog(id, dlg);
		// 화면에 보여지기 전에 설정할 내용 여기에...
	}


	// AlertDialog에 사용할 ListView로 구성된 커스텀 view 생성(사용안함)
	/*private View createCustomView() {
		LinearLayout linearLayoutView = new LinearLayout(this);

		final ListView listview = new ListView(this);
		ArrayAdapter<String> aa = new ArrayAdapter<String> (this, R.layout.listlayout_simple_small, listEdit);

		listview.setAdapter(aa);
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//리스트의 몇번째 항목 클릭후 실행사항
				if(position == 2) {
					Userlist.remove(listposition);
					ulaa.notifyDataSetChanged();				
					//전역변수 쓰레기들 나중에 고치기
				}
				else if(position == 0){
					User CurUser = Userlist.get(listposition);

					String ID = CurUser.getID();

					//Toast.makeText(this, ID, Toast.LENGTH_LONG).show();

					Intent UserInfoIntent = new Intent(UserList.this, UserInfo.class);
					UserInfoIntent.putExtra("UserID", ID);
					startActivityForResult(UserInfoIntent, 2);
				}else if(position == 1){
					User CurUser = Userlist.get(listposition);

					String ID = CurUser.getID();

					//Toast.makeText(this, ID, Toast.LENGTH_LONG).show();

					Intent ScoreListIntent = new Intent(UserList.this, ScoreList.class);
					ScoreListIntent.putExtra("UserID", ID);
					startActivity(ScoreListIntent);
				}
				dlg.dismiss(); //// <- 그 dlg를 여기서 없앰(dlg의 리스트 클릭할경우 없앰)
			}			
		});
	}
	 */

	//-----------------------------------------------------------------
	//		initButtons - 각 버튼 설정
	//-----------------------------------------------------------------
	private void initButtons(){
		int[] buttons = {R.id.ScoreList_Align_VersionBtn, R.id.ScoreList_Align_TitleBtn, R.id.ScoreList_Align_LevelBtn, R.id.ScoreList_Align_ScoreBtn, R.id.ScoreList_Align_DateBtn};
		comparators = new ArrayList<Comparator<Score>>();

		// sort_id만 정순, 나머진 역순
		comparators.add(new Comparator<Score>() {
			// version:  version, sort_id, (difficulty)
			public int compare(Score object1, Score object2) {
				int key1 = -object1.version * 10000 + object1.sort_id;
				int key2 = -object2.version * 10000 + object2.sort_id;
				if (key1 > key2) return 1;
				return -1;
			}
		});
		comparators.add(new Comparator<Score>() {
			// title:  sort_id, difficulty
			public int compare(Score object1, Score object2) {
				if (object1.sort_id > object2.sort_id) return 1;
				return -1;
			}
		});
		comparators.add(new Comparator<Score>() {
			// level: level, version, sort_id
			public int compare(Score object1, Score object2) {
				int key1 = -object1.level * 100000 -object1.version * 10000 + object1.sort_id;
				int key2 = -object2.level * 100000 -object2.version * 10000 + object2.sort_id;
				if (key1 > key2) return 1;
				return -1;
			}
		});
		comparators.add(new Comparator<Score>() {
			// score: score, sort_id
			public int compare(Score object1, Score object2) {
				int key1 = -object1.score;
				int key2 = -object2.score;

				int key1_1 = object1.sort_id;
				int key2_1 = object2.sort_id;

				if(key1 > key2){
					return 1;
				}else if(key1 == key2){
					if(key1_1 > key2_1){
						return 1;
					}
				}
				return -1;
			}
		});
		comparators.add(new Comparator<Score>() {
			private final Collator collator = Collator.getInstance();
			public int compare(Score object1, Score object2) {
				int dateCompare = collator.compare(object1.date, object2.date);
				if (dateCompare != 0) return -dateCompare;
				//Log.e("", String.format("%d", dateCompare));
				//Log.e("YYYYYYYYYYYYYYYY", String.format("%d", dateCompare));

				int key1 = -object1.version * 10000 + object1.sort_id; 
				int key2 = -object2.version * 10000 + object2.sort_id;
				//Log.e("YYYYYYYYYYYYYYYY", String.format("%d %d %d %d", key1, dateCompare, ));
				if(key1 > key2) return 1;
				else if(key1 == key2)return 0;
				else return -1;
			}
		});

		for (int i = 0; i < buttons.length; i++) {
			final int index = i;
			TextView textView = (TextView)findViewById(buttons[i]);
			textView.setOnClickListener(new OnClickListener(){
				public void onClick(View v){
					if(exist==0){
						listview.setAdapter(slaa);
						Collections.sort(Scorelist, comparators.get(index));
						//Collections.reverse(Scorelist);
						slaa.notifyDataSetChanged();
					}else{
						listview.setAdapter(slaa_e);
						Collections.sort(Scorelist_Exist, comparators.get(index));
						//Collections.reverse(Scorelist);
						slaa_e.notifyDataSetChanged();
					}
				}
			});
		}

	}

	
	//-------------------------------------------
	//		Score Class
	//-------------------------------------------
	public class Score{
		public String artist;
		//		public String bpm;
		public String title;
		public int version;
		//		public int copious_offset;
		//		public int flag;
		public int sort_id;
		public String localization;

		public String diffculty;
		public int score;
		public int level;
		//		public String notes;
		public String rating;
		//public String time;
		public String date;

		public String image_name;
		public String image_path;

		public Score(
				String _artist,
				//				String _bpm,
				String _title,
				int _version,
				//				int _copious_offset,
				//				int _flag, 
				int _sort_id,

				String _difficulty,
				int _score,
				int _level,
				//				String _notes,
				String _rating,
				//String _time,
				String _date,
				String _localization){

			this.artist = _artist;
			//			this.bpm = _bpm;
			this.title = _title;
			this.version = _version;
			//			this.copious_offset = _copious_offset;
			//			this.flag = _flag;
			this.sort_id = _sort_id;
			this.diffculty = _difficulty;
			this.score = _score;
			this.level = _level;
			//			this.notes = _notes;
			this.rating = _rating;
			//this.time = _time;
			this.date = _date;
			this.localization = _localization;
//			this.image_name = "icon" + String.format("%04d", _sort_id);
			this.image_name = String.format("%04d", _sort_id) + ".png";
		}

	}

	//타이틀바 색상 바꾸기
	private void doTitlebar()
	{
		View titleView = getWindow().findViewById(android.R.id.title);
		if (titleView != null)
		{
			ViewParent parent = titleView.getParent();
			if (parent != null && (parent instanceof View))
			{
				View parentView = (View) parent;
				parentView.setBackgroundResource(R.drawable.titlebar);
			}
		}
	}
}