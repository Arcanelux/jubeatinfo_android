package lhy.JubeatInfo;

import java.util.ArrayList;

import lhy.JubeatInfo.UserList.User;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class UserListArrayAdapter extends ArrayAdapter<User>{		
	private ArrayList<User> mUser;
	private Context mContext;
	private int mResource;
	private LayoutInflater mInflater;

	public UserListArrayAdapter(Context context, int LayoutResource, ArrayList<User> User){
		super(context, LayoutResource, User);
		this.mUser = User;
		this.mContext = context;
		this.mResource = LayoutResource;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		User mCurUser = mUser.get(position);

		if(convertView == null)
			convertView = mInflater.inflate(mResource, null);

		ImageView JubilityIcon = (ImageView)convertView.findViewById(R.id.UserList_JubilityIcon);
		TextView JubeatInfoName = (TextView)convertView.findViewById(R.id.UserList_JubeatInfoName);
		TextView CardName = (TextView)convertView.findViewById(R.id.UserList_CardName);
		TextView GroupName = (TextView)convertView.findViewById(R.id.UserList_GroupName);
		TextView UserTitle = (TextView)convertView.findViewById(R.id.UserList_UserTitle);
		
		/*
			//유저리스트의 간략정보(카드이름, 마지막플레이)를 표현하기위한  JSONObject 생성,사용
			String UserID = ID;
			String FrontUrl = "http://j.ubeat.info/copious/";
			String BackUrl = "?fmt=json&lang=";
			String Local = "ko";
			String Url = FrontUrl + UserID + BackUrl + Local;

			if(ID!="Add User"){
				JSONObject CurUser = JsonFunc.getJSONfromURL(Url);
				try{
					//각 정보 TextView의 변수 연결
					CardName.setText(CurUser.getString("card_name"));
					LastPlay.setText(CurUser.getString("last_date"));
				}catch(JSONException e){
					Log.e("log_tag", "Error parsing data "+e.toString());
				}
			}
			////////////////////////////////////////////////////////////////////////
		 */

		JubeatInfoName.setText(mCurUser.getID());
		CardName.setText(mCurUser.getCardName());
		if(mCurUser.getGroupName()=="null"){
			GroupName.setText("No Group");
		}else{
			GroupName.setText(mCurUser.getGroupName());
		}
		UserTitle.setText(mCurUser.getTitle());
		String JubilityIconUrl = mCurUser.getJubilityIconUrl();

		final ImageDownloader imageDownloader = new ImageDownloader();
		imageDownloader.download(JubilityIconUrl, JubilityIcon);
		
		return convertView;
	}
}