package lhy.JubeatInfo;

import java.util.ArrayList;


import lhy.JubeatInfo.ScoreList.Score;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ScoreListArrayAdapter extends ArrayAdapter<Score>{
	private final String TAG = "JubeatInfo_ScoreListArrayAdapter";
	private final String TAG2 = "ScoreListArrayAdapter";
	private ArrayList<Score> mScore;
	private Context mContext;
	private int mResource;
	private LayoutInflater mInflater;
	int[] backgrounds = {0, R.drawable.v1, R.drawable.v2, R.drawable.v3, R.drawable.v4, R.drawable.v5, R.drawable.v6, R.drawable.v6};

	public ScoreListArrayAdapter(Context context, int LayoutResource, ArrayList<Score> Score){
		super(context, LayoutResource, Score);
		this.mScore = Score;
		this.mContext = context;
		this.mResource = LayoutResource;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		Score mCurScore = mScore.get(position);

		if(convertView == null)
			convertView = mInflater.inflate(mResource, null);

		ImageView ivSongIcon = (ImageView)convertView.findViewById(R.id.ScoreList_SongIcon);
		TextView Title = (TextView)convertView.findViewById(R.id.ScoreList_Title);
		//TextView Artist = (TextView)convertView.findViewById(R.id.ScoreList_Artist);
		//TextView Difficulty;
		TextView LastDate = (TextView)convertView.findViewById(R.id.ScoreList_LastDate);
		TextView Level = (TextView)convertView.findViewById(R.id.ScoreList_Level);
		TextView Difficulty = (TextView)convertView.findViewById(R.id.ScoreList_Difficulty);
		TextView Rating = (TextView)convertView.findViewById(R.id.ScoreList_Rating);
		TextView Score = (TextView)convertView.findViewById(R.id.ScoreList_Score);
		TextView Localization = (TextView)convertView.findViewById(R.id.ScoreList_LocalTitle);

		Title.setText(mCurScore.title);
		//Artist.setText(mCurScore.artist);
		if((mCurScore.date).equals("")){
			LastDate.setText("No Play");
		} else {
			LastDate.setText(mCurScore.date);
		}
		Level.setText(String.format("%2d", mCurScore.level));
		Difficulty.setText(mCurScore.diffculty);
		Rating.setText(mCurScore.rating);
		if(mCurScore.score!=0){
			Score.setText(String.format("%d", mCurScore.score));
		} else {
			Score.setText("");
		}
		if(mCurScore.localization!="null"){
			Localization.setText(mCurScore.localization);
		} else {
			Localization.setText("");
		}

		ivSongIcon.setImageResource(getImageId(this.mContext, mCurScore.image_name));

		int background = backgrounds[mCurScore.version];
		convertView.setBackgroundResource(background);	

		//		// 내부이미지가 없을경우 ImageDownloader에 저장될이름과 Context전달하여 파일저장
		//		String imgPath = Val.local_path + mCurScore.image_name;
		//		Bitmap bm = BitmapFactory.decodeFile(imgPath);
		////		Log.d(TAG, "imgPath : " + imgPath);
		//
		//		if(bm!=null){
		//			ivSongIcon.setImageBitmap(bm);
		//			if(Val.D) Log.d(TAG2, "savedImage load : " + mCurScore.image_name);
		//		} else{
		//			final ImageDownloader imageDownloader = new ImageDownloader();
		//			imageDownloader.download(Val.image_path + mCurScore.image_name, ivSongIcon, mCurScore.image_name, mContext);
		//		}

		return convertView;
	}

	public static int getImageId(Context context, String imageName) {
		return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
	}

	public void findScoe(ArrayList<Score> mScore){
		for(int i = 0; i<mScore.size(); i++){
			if(mScore.get(i).score > 90){
				//Insert code here
			}
		}
	}
}