package lhy.JubeatInfo;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class UserList extends ListActivity{
	private ArrayList<User> Userlist;
	private UserListArrayAdapter ulaa = null;

	private String[] listEdit = {"User Info", "Score", "Delete"};
	int listposition;
	AlertDialog dlg;

	@Override
	public void onCreate(Bundle savedInstanceState){
		doTitlebar();//타이틀바 색상 바꾸기 함수
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.userlist);

		Userlist = new ArrayList<User>();


		//---------------------------------------------------------------------------
		//		SharedPreferences에서 유저정보 불러옴
		//---------------------------------------------------------------------------
		SharedPreferences pref = getSharedPreferences("size", 0);
		int UserlistSize = pref.getInt("Size", 0);

		for(int i=0; i<UserlistSize; i++){
			String index = String.format("%02d", i);
			pref = getSharedPreferences(index, 0);
			String ID = pref.getString("ID", "");
			String CardName = pref.getString("CardName", "");
			String GroupName = pref.getString("GroupName", "");
			String Title = pref.getString("Title", "");
			String JubilityIconUrl = pref.getString("JubilityIconUrl", "");

			User Add = new User(ID, CardName, Title, GroupName, JubilityIconUrl);

			Userlist.add(Add);
		}


		/*
		//테스트용
		User Test = new User("3rdpub", "3RDPUBS", "ABCD", "2011-99-99", "http://j.ubeat.info/images/jubility/copious6.2.png");

		Userlist.add(Test);
		Userlist.add(Test);
		Userlist.add(Test);
		Userlist.add(Test);
		Userlist.add(Test);
		Userlist.add(Test);
		//테스트용
		 */

		/* 그냥 어댑터
		adapter = new ArrayAdapter<String>(this, R.layout.listlayout_simple, list);
		setListAdapter(adapter);
		 */



		//------------------------------------------------------
		//		UserList로 CustomArrayAdapter선언/초기화
		//			본 Activity Adapter로 ulaa지정
		//------------------------------------------------------
		ulaa = new UserListArrayAdapter(this, R.layout.userlist_row, Userlist);
		//각 row의 레이아웃, 전달할 리스트 인자로 전달
		setListAdapter(ulaa);


		//------------------------------------------------------
		//		AddUser, News TextView Button
		//------------------------------------------------------
		TextView AddTextBtn = (TextView)findViewById(R.id.AddUserText);
		AddTextBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				Intent intent = new Intent(UserList.this, UserAdd.class);
				startActivityForResult(intent, 1);
			}
		});

		TextView NewsTextBtn = (TextView)findViewById(R.id.NewsText);
		NewsTextBtn.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				Intent intent = new Intent(UserList.this, Notice.class);
				startActivity(intent);
			}
		});


		//----------------------------------------------------------
		//		Background Image설정 / 비율유지안됨(나중에 수정)
		//----------------------------------------------------------
		View backgroundimage = findViewById(R.id.userlist_layout);
		Drawable background = backgroundimage.getBackground();
		background.setAlpha(40);

	}

	//----------------------------------------------------
	//		onPause - SharedPreferences 저장
	//----------------------------------------------------
	public void onPause(){
		super.onPause();
		SharedPreferences pref = getSharedPreferences("size", 0);
		SharedPreferences.Editor editor = pref.edit();
		int UserlistSize = Userlist.size();
		editor.putInt("Size", UserlistSize);
		editor.commit();
		for(int i=0; i<Userlist.size(); i++){
			String index = String.format("%02d", i);
			pref = getSharedPreferences(index, 0);
			editor = pref.edit();

			editor.putString("ID", Userlist.get(i).ID);
			editor.putString("CardName", Userlist.get(i).CardName);
			editor.putString("GroupName", Userlist.get(i).GroupName);
			editor.putString("Title", Userlist.get(i).Title);
			editor.putString("JubilityIconUrl", Userlist.get(i).JubilityIconUrl);

			editor.commit();
		}
	}


	//---------------------------------------------------------------------------
	//		onActivityResult - UserAdd, UserInfo에서 나왔을때 받아온 데이터 적용
	//---------------------------------------------------------------------------
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK){
			switch (requestCode) {
			case 1: {
				//UserAdd에서 받아왔을 때
				String ID = data.getStringExtra("UserID");
				String CardName = data.getStringExtra("CardName");
				String GroupName = data.getStringExtra("GroupName");
				String Title = data.getStringExtra("Title");
				String JubilityIconUrl = data.getStringExtra("JubilityIconUrl");

				User Add = new User(ID, CardName, Title, GroupName, JubilityIconUrl);
				Userlist.add(Add);
				ulaa.notifyDataSetChanged();
			}	
			break;
			case 2: {
				//UserInfo를 보고 나왔을 때(간략정보 업데이트)
				String CardName = data.getStringExtra("CardName");
				String GroupName = data.getStringExtra("GroupName");
				String Title = data.getStringExtra("Title");
				String JubilityIconUrl = data.getStringExtra("JubilityIconUrl");

				Context context = this.getBaseContext();
				Drawable JubilityIcon = ImageOperations(context, JubilityIconUrl, "JubilityIcon.png");
				ImageView JubilityIconView = new ImageView(context);
				JubilityIconView = (ImageView)findViewById(R.id.UserList_JubilityIcon);
				JubilityIconView.setImageDrawable(JubilityIcon);

				String ID = Userlist.get(listposition).ID;
				Userlist.remove(listposition);
				Userlist.add(listposition, new User(ID, CardName, Title, GroupName, JubilityIconUrl));

				ulaa.notifyDataSetChanged();
			}
			break;
			default:
				break;
			}
		}
	}

	//----------------------------------------------------------------------
	//		Drwable ImageOperations - 그림출력을 위한 이미지 주소 넣어놓기
	//----------------------------------------------------------------------
	private Drawable ImageOperations(Context ctx, String url, String saveFilename) {
		try {
			URL imageUrl = new URL(url);
			InputStream is = (InputStream) imageUrl.getContent();
			Drawable d = Drawable.createFromStream(is, "src");
			return d;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}


	//----------------------------------------------------------
	//		onListItemClick - 리스트 아이템 클릭했을 때
	//----------------------------------------------------------
	@Override
	protected void onListItemClick (ListView l, View v, int position, long id){
		/*///////////////리스트아이템에 버튼만들고 기능추가할때까지 봉인
		//그냥 클릭때
		super.onListItemClick(l, v, position, id);
		//Toast.makeText(this, list.get(position), Toast.LENGTH_SHORT).show();

		//String ID = getListView().getItemAtPosition(position).toString(); //객체아닐때 구하던법
		User CurUser = (User)l.getItemAtPosition(position);

		String ID = CurUser.getID();

		//Toast.makeText(this, ID, Toast.LENGTH_LONG).show();

		Intent UserInfoIntent = new Intent(UserList.this, UserInfo.class);
		UserInfoIntent.putExtra("UserID", ID);
		listposition = position;
		startActivityForResult(UserInfoIntent, 2);
		 */
		showDialog(0); ////팝업창 만드는곳////
		listposition = position;
		//현재 포지션 전역변수선언

		/*
		//롱클릭 - 임시봉인
		l.setOnItemLongClickListener(new OnItemLongClickListener(){
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				showDialog(0); ////팝업창 만드는곳////
				listposition = arg2;
				//현재 포지션 전역변수선언(커스텀뷰로 전달 어떻게함?)
				return true;
			}
		});
		 */
	}

	//----------------------------------------------------------------------------------------
	//		onCreateDialog - 만들어질 Dialog의 형태 결정 <- 이 함수로 만들어짐(dlg = 팝업창)
	//----------------------------------------------------------------------------------------
	@Override
	protected Dialog onCreateDialog(int id){
		super.onCreateDialog(id);
		//스위치문으로 여러개의 Dialog만들어놓고 필요한것을 부를수있음
		switch(id){
		case 0:
			//현재는 하나뿐
			dlg = new AlertDialog.Builder(this)
			//.setTitle("Edit UserInfo")
			//.setMessage("UserInfo")
			.setView(createCustomView())
			.create();
			break;
		default:
			dlg = null;
		}
		return dlg;
	}

	//-----------------------------------------------------------------------
	//		onPrepareDialog  
	//			처음 생성된 AlertDialog 객체이건 재사용되는 AlerDialog 객체이건
	//			AlertDialog객체가 화면에 표시 될때 마다 항상 호출 됨
	//-----------------------------------------------------------------------
	@Override
	public void onPrepareDialog(int id, Dialog dlg) {
		super.onPrepareDialog(id, dlg);
		// 화면에 보여지기 전에 설정할 내용 여기에...
	}

	//--------------------------------------------------------------------------------
	//		createCustomView() - AlertDialog에 사용할 ListView로 구성된 커스텀 view 생성
	//--------------------------------------------------------------------------------
	private View createCustomView() {
		LinearLayout linearLayoutView = new LinearLayout(this);

		final ListView listview = new ListView(this);
		ArrayAdapter<String> aa = new ArrayAdapter<String> (this, R.layout.listlayout_simple_small, listEdit);

		listview.setAdapter(aa);
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//리스트의 몇번째 항목 클릭후 실행사항
				if(position == 2) {
					//3번째항목. Delete
					Userlist.remove(listposition);
					ulaa.notifyDataSetChanged();
				}
				else if(position == 0){
					//1번째항목. UserInfo
					User CurUser = Userlist.get(listposition);

					String ID = CurUser.getID();
					//Toast.makeText(this, ID, Toast.LENGTH_LONG).show();

					Intent UserInfoIntent = new Intent(UserList.this, UserInfo.class);
					UserInfoIntent.putExtra("UserID", ID);
					startActivityForResult(UserInfoIntent, 2);
				}else if(position == 1){
					//2번째항목. UserScore
					User CurUser = Userlist.get(listposition);

					String ID = CurUser.getID();
					//Toast.makeText(this, ID, Toast.LENGTH_LONG).show();

					Intent ScoreListIntent = new Intent(UserList.this, ScoreList.class);
					ScoreListIntent.putExtra("UserID", ID);
					startActivity(ScoreListIntent);
				}
				dlg.dismiss();		//Dialog없애기(뭐든지 클릭 후)
			}			
		});

		//TextView tv = new TextView(this);
		//tv.setText("  Custom View 영역");

		linearLayoutView.setOrientation(LinearLayout.VERTICAL);
		//		linearLayoutView.addView(tv);
		linearLayoutView.addView(listview);

		return linearLayoutView;
	}

	//-----------------------------------------------------------
	//		doTitlebar() - 타이틀바 색상 바꾸기
	//-----------------------------------------------------------
	private void doTitlebar()
	{
		View titleView = getWindow().findViewById(android.R.id.title);
		if (titleView != null)
		{
			ViewParent parent = titleView.getParent();
			if (parent != null && (parent instanceof View))
			{
				View parentView = (View) parent;
				parentView.setBackgroundResource(R.drawable.titlebar);
			}
		}
	}


	
	//----------------------------------------------------------
	//		User class
	//----------------------------------------------------------
	public class User implements Serializable{
		private static final long serialVersionUID = 1L;
		private String ID;
		private String CardName;
		private String Title;
		private String GroupName;
		private String JubilityIconUrl;

		public User(String _ID, String _CardName, String _Title, String _GroupName, String _JubilityIconUrl){
			this.ID = _ID;
			this.CardName = _CardName;
			this.Title = _Title;
			this.GroupName = _GroupName;
			this.JubilityIconUrl = _JubilityIconUrl;
		}

		public String getID(){
			return ID;
		}
		public String getCardName(){
			return CardName;
		}
		public String getTitle(){
			return Title;
		}
		public String getGroupName(){
			return GroupName;
		}
		public String getJubilityIconUrl(){
			return JubilityIconUrl;
		}
	}
}